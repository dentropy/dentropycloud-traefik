#!/bin/bash
set -a
. .env
set +a
MY_CURRENT_PATH=`pwd`

# Declare an array of string with type
declare -a StringArray=(
  "/apps/misskey"
  "/apps/trilium"
  "/apps/peertube"
  "/apps/zincsearch"
  "/apps-manual-deployment/monica"
  "/apps-networking/traefik-secure"
)
 
# Iterate the string array using for loop
for SETUP_DIR in "${StringArray[@]}"; do
  cd $MY_CURRENT_PATH$SETUP_DIR
  echo `pwd`
  bash ./setup.sh
  cd "$MY_CURRENT_PATH"
done

cd "$MY_CURRENT_PATH"

bash ./generate_docker_compose.sh

bash ./up.sh
