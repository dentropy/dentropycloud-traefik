#!/bin/bash

source .env
cd $VOLUME_DIR
./stop.sh
docker run --rm \
  -v trilium:/data \
  -v ${VOLUME_DIR}/backups:/backup ubuntu bash \
  -c "cd /data && tar cvf /backup/trilium-$(date +"%m_%d_%Y").tar ."
docker run --rm \
  -v peertube-assets:/data \
  -v ${VOLUME_DIR}/backups:/backup ubuntu bash \
  -c "cd /data && tar cvf /backup/peertube-assets-$(date +"%m_%d_%Y").tar ."
echo "Now backup ${VOLUME_DIR} somewhere and run the ./start script"

for d in */ ; do
    echo "$d"
    if [  "$d" != "backups/"  ];
    then
      echo "$d is not backups/"
      tar cvf /backup/$d-$(date +"%m_%d_%Y").tar $d
    else 
      echo "$d is backups/"
    fi
done