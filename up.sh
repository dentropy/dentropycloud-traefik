#!/bin/bash

# App List: "audioserve" "ELK" "filebrowser" "jellyfin" "misskey" "monica" "qbittorrent" "static" "syncthing" "trilium" "whoami-test"
bash generate_docker_compose.sh
startApps=(
    "./apps-networking/traefik-secure" 
    "./apps-networking/yacht"
)

MY_CURRENT_PATH=`pwd`
for value in ${!startApps[@]}
do
    echo ${startApps[$value]}
    cd ${startApps[$value]} && docker-compose up -d
    cd "$MY_CURRENT_PATH"
done
