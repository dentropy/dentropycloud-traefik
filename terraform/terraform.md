## Terraform Commands

``` bash
terraform init
terraform show
terraform plan
terraform plan -var-file ./dentropycloud.tfvars
terraform apply
terraform apply -var-file ./dentropycloud.tfvars
terraform apply -destroy
terraform apply -destroy -var-file ./dentropycloud.tfvars
```
