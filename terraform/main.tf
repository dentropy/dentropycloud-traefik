
resource "vultr_vpc" "dentropycloud_vpc" {
  description    = "dentropycloud"
  region         = "yto"
  v4_subnet      = "10.22.96.0"
  v4_subnet_mask = 20
}

resource "vultr_instance" "dentropycloud_host" {
  plan             = "vhp-1c-2gb-intel"
  region           = "yto"
  os_id            = 477
  label            = "HOST_DentropyCloud"
  tags             = ["terraform"]
  hostname         = "dentropycloud"
  vpc_ids          = [vultr_vpc.dentropycloud_vpc.id]
  enable_ipv6      = false
  backups          = "disabled"
  ddos_protection  = false
  activation_email = false
  user_data = "${file("cloud_configs/dentropycloud-prod.yml")}"
}

#    resource "vultr_block_storage" "dentropycloud_blockstorage" {
#        size_gb        = 10
#        region         = "yto"
#        attached_to_instance = vultr_instance.dentropycloud_host.id
#       # mount_id = "dentropycloud"
#        live = true
#    }

resource "vultr_dns_domain" "my_domain" {
    domain = "${var.dentropycloud_domain_name}"
    ip = vultr_instance.dentropycloud_host.main_ip
}

resource "vultr_dns_record" "www_subdomain" {
    domain = "${var.dentropycloud_domain_name}"
    name = "www"
    data = vultr_instance.dentropycloud_host.main_ip
    type = "A"
    ttl  = 60
}

resource "vultr_dns_record" "test_subdomain" {
    domain = "${var.dentropycloud_domain_name}"
    name = "test"
    data = vultr_instance.dentropycloud_host.main_ip
    type = "A"
    ttl  = 60
}

resource "vultr_dns_record" "audiobooks_subdomain" {
    domain = "${var.dentropycloud_domain_name}"
    name = "audiobooks"
    data = vultr_instance.dentropycloud_host.main_ip
    type = "A"
    ttl  = 60
}

resource "vultr_dns_record" "filebrowser_subdomain" {
    domain = "${var.dentropycloud_domain_name}"
    name = "filebrowser"
    data = vultr_instance.dentropycloud_host.main_ip
    type = "A"
    ttl  = 60
}

resource "vultr_dns_record" "jellyfin_subdomain" {
    domain = "${var.dentropycloud_domain_name}"
    name = "jellyfin"
    data = vultr_instance.dentropycloud_host.main_ip
    type = "A"
    ttl  = 60
}

resource "vultr_dns_record" "media_subdomain" {
    domain = "${var.dentropycloud_domain_name}"
    name = "media"
    data = vultr_instance.dentropycloud_host.main_ip
    type = "A"
    ttl  = 60
}

resource "vultr_dns_record" "misskey_subdomain" {
    domain = "${var.dentropycloud_domain_name}"
    name = "misskey"
    data = vultr_instance.dentropycloud_host.main_ip
    type = "A"
    ttl  = 60
}

resource "vultr_dns_record" "monica_subdomain" {
    domain = "${var.dentropycloud_domain_name}"
    name = "monica"
    data = vultr_instance.dentropycloud_host.main_ip
    type = "A"
    ttl  = 60
}

resource "vultr_dns_record" "peertube_subdomain" {
    domain = "${var.dentropycloud_domain_name}"
    name = "peertube"
    data = vultr_instance.dentropycloud_host.main_ip
    type = "A"
    ttl  = 60
}

resource "vultr_dns_record" "videos_subdomain" {
    domain = "${var.dentropycloud_domain_name}"
    name = "videos"
    data = vultr_instance.dentropycloud_host.main_ip
    type = "A"
    ttl  = 60
}

resource "vultr_dns_record" "syncthing_subdomain" {
    domain = "${var.dentropycloud_domain_name}"
    name = "syncthing"
    data = vultr_instance.dentropycloud_host.main_ip
    type = "A"
    ttl  = 60
}

resource "vultr_dns_record" "trilium_subdomain" {
    domain = "${var.dentropycloud_domain_name}"
    name = "trilium"
    data = vultr_instance.dentropycloud_host.main_ip
    type = "A"
    ttl  = 60
}

resource "vultr_dns_record" "wiki_subdomain" {
    domain = "${var.dentropycloud_domain_name}"
    name = "wiki"
    data = vultr_instance.dentropycloud_host.main_ip
    type = "A"
    ttl  = 60
}

resource "vultr_dns_record" "zincsearch_subdomain" {
    domain = "${var.dentropycloud_domain_name}"
    name = "zincsearch"
    data = vultr_instance.dentropycloud_host.main_ip
    type = "A"
    ttl  = 60
}

resource "vultr_dns_record" "yacht_subdomain" {
    domain = "${var.dentropycloud_domain_name}"
    name = "yacht"
    data = vultr_instance.dentropycloud_host.main_ip
    type = "A"
    ttl  = 60
}

resource "vultr_dns_record" "nextcloud_subdomain" {
    domain = "${var.dentropycloud_domain_name}"
    name = "nextcloud"
    data = vultr_instance.dentropycloud_host.main_ip
    type = "A"
    ttl  = 60
}

resource "vultr_dns_record" "photoprisim_subdomain" {
    domain = "${var.dentropycloud_domain_name}"
    name = "photoprisim"
    data = vultr_instance.dentropycloud_host.main_ip
    type = "A"
    ttl  = 60
}

resource "vultr_dns_record" "searx_subdomain" {
    domain = "${var.dentropycloud_domain_name}"
    name = "searx"
    data = vultr_instance.dentropycloud_host.main_ip
    type = "A"
    ttl  = 60
}