#!/bin/bash
lxc launch ubuntu:focal my-test --config=user.user-data="$(cat $1)"
lxc shell my-test
lxc stop my-test
lxc rm my-test