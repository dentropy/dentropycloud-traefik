variable "vultr_api_key" {
  type = string
}

variable "dentropycloud_domain_name" {       
  type = string
}

# variable "dentropycloud_host_ip" {       
#   type = string
# }

# variable "remote_state_address" {
#   type = string
#   default = "https://gitlab.com/api/v4/projects/37385176/terraform/state/main"
# }

# variable "gitlab_username" {
#   type = string
# }

# variable "gitlab_token" {
#   type = string
# }

