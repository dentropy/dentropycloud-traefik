#!/bin/bash

set -a
. .env
set +a

cat ./terraform/cloud_configs/dentropycloud.yml | envsubst > ./terraform/cloud_configs/dentropycloud-prod.yml
