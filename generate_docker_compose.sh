#!/bin/bash
set -a
. .env
export DOCKERHOST=$(ifconfig | grep -E "([0-9]{1,3}\.){3}[0-9]{1,3}" | grep -v 127.0.0.1 | awk '{ print $2 }' | cut -f2 -d: | head -n1)


export current_ip=`curl ifconfig.co`
# Generate 

htpasswd -b -c ./TmpPassword.txt $AUTH_USERNAME $AUTH_PASSWORD
sed -i s/\\$/\\$\\$/g ./TmpPassword.txt
AUTH_CREDS=`cat ./TmpPassword.txt`
echo $AUTH_CREDS
rm ./TmpPassword.txt

# Generate

htpasswd -b -c ./TmpPassword.txt $ADMIN_AUTH_USERNAME $ADMIN_AUTH_PASSWORD
sed -i s/\\$/\\$\\$/g ./TmpPassword.txt
ADMIN_CREDS=`cat ./TmpPassword.txt`
echo $ADMIN_CREDS
rm ./TmpPassword.txt


set +a
MY_CURRENT_PATH=`pwd`

# Template Everything

for directory in ./apps/* ; do
    echo "$directory"
    cd $directory
    cat docker-compose.yml.bak | envsubst > docker-compose.yml
    cd $MY_CURRENT_PATH
done
for directory in ./apps-manual-deployment/* ; do
    echo "$directory"
    cd $directory
    cat docker-compose.yml.bak | envsubst > docker-compose.yml
    cd $MY_CURRENT_PATH
done
for directory in ./apps-networking/* ; do
    echo "$directory"
    cd $directory
    cp docker-compose.yml.bak docker-compose.yml
    cat docker-compose.yml | envsubst > test.txt && mv test.txt docker-compose.yml
    cd $MY_CURRENT_PATH
done
for directory in ./apps-umbrel/* ; do
    echo "$directory"
    cd $directory
    cp docker-compose.yml.bak docker-compose.yml
    cat docker-compose.yml | envsubst > test.txt && mv test.txt docker-compose.yml
    cd $MY_CURRENT_PATH
done