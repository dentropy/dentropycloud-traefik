
## Project Website

[MediaGoblin](https://mediagoblin.org/)

## Source docker container

[vimagick/mediagoblin - Docker Image | Docker Hub](https://hub.docker.com/r/vimagick/mediagoblin/)

## Instructions

The default username and password are set in the docker-compose.yml file so please change them there before putting this on the public internet.
