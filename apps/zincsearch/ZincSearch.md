# ZincSearch

No working volumes because of permission problems

* [ZincSearch](https://docs.zincsearch.com/installation/)
* [ZincSearch – lightweight alternative to Elasticsearch written in Go | Hacker News](https://news.ycombinator.com/item?id=32938304)

## Permission issue

[ZINC\_DATA\_PATH is not writable · Issue #512 · zinclabs/zinc](https://github.com/zinclabs/zinc/issues/512)

``` bash
# Does not work
mkdir -p /srv/zincsearch
mkdir -p /srv/zincsearch
chmod a+w /srv/zincsearch
```