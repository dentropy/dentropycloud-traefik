#!/bin/sh
set -a
. ../../.env
set +a
echo "$VOLUME_DIR"
echo "Checking for Misskey's .config directory"
if [ -d "$VOLUME_DIR/misskey/.config" ] 
then
    echo "Directory $VOLUME_DIR/misskey/.config exists." 
else
    echo "Error: Directory $VOLUME_DIR/misskey/.config does not exists."
    mkdir $VOLUME_DIR/misskey/
    cp -r .config $VOLUME_DIR/misskey/
    echo $MY_DOMAIN
    envsubst < $VOLUME_DIR/misskey/.config/example.yml
    cp $VOLUME_DIR/misskey/.config/example.yml $VOLUME_DIR/misskey/.config/default.yml
fi