#!/bin/bash

# Designed For Debian 11, will work with Ubuntu

sudo apt-get -y update
sudo apt-get install -y tmux
sudo apt-get install -y git
sudo apt-get install -y zsh
sudo apt-get install -y htop
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo apt-get install -y docker-compose
sudo systemctl start docker
sudo systemctl enable docker
rm get-docker.sh
