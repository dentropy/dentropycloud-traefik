# dentropycloud-traefik

The goal of [dentropycloud](https://publish.obsidian.md/ddaemon/dentropydaemon-wiki/Projects/DentropyCloud) is to automate as much of the homelab deployment process as possible. Just set this project up in a VM and you can run a [list of applications](https://publish.obsidian.md/ddaemon/dentropydaemon-wiki/Projects/DentropyCloud/DentropyCloud+Docs/DentropyCloud+App+List). The main problems I am trying to solve is DNS management, authentication, backup and restore.

## Manual Install

Set DNS Records as documented [here](https://publish.obsidian.md/ddaemon/dentropydaemon-wiki/Projects/DentropyCloud/DentropyCloud+Docs/Ports%2C+IP's%2C+Network+and+DNS+-+DentropyCloud).

**Fresh Install Script**

``` bash
# Remember UFW is a thing
sudo su
sudo apt-get -y update
sudo apt-get install -y tmux
sudo apt-get install -y git
sudo apt-get install -y zsh
sudo apt-get install -y htop
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo apt-get install -y docker-compose
sudo systemctl start docker
sudo systemctl enable docker
rm get-docker.sh
sudo usermod -aG docker $USER
```

**RUN COMMANDS ONE AT A TIME**

```
git clone https://gitlab.com/dentropy/dentropycloud-traefik.git

cd dentropycloud-traefik

cp env_example .env

nano .env 
# Edit the required variables at the top

bash ./setup.sh
```

## Terraform Install


**RUN ON YOUR LOCAL PC AND BACKUP FOLDER**

``` bash
git clone https://gitlab.com/dentropy/dentropycloud-traefik.git

cd dentropycloud-traefik

cp env_example .env

nano .env

./template-terraform.sh

cd terraform

cp dentropycloud.tfvars.temp dentropycloud.tfvars

nano dentropycloud.tfvars

# Set vultr_api_key
# Set dentropycloud_domain_name as your root domain name
# Remember to set DNS name service records 
#   to ns1.vultr.com and ns2.vultr.com

terraform init

terraform plan -var-file ./dentropycloud.tfvars

terraform apply -var-file ./dentropycloud.tfvars
```
